<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 14pt; background-color: #EEE">
                <div style="background-color: blue; color: white;">
                    <h2>COLD  WEAPON</h2>
                </div>
                <table border="3">
                    <tr bgcolor="#2E9AFE">
                        <th>Knife ID</th>
                        <th>Name</th>
                        <th>Country</th>
                        <th>Type</th>
                        <th>Blade length (cm)</th>
                        <th>Blade width (mm)</th>
                        <th>Blade material</th>
                        <th>Handle material</th>
                        <th>Has rib of rigidity</th>
                        <th>Is collectible</th>
                    </tr>

                    <xsl:for-each select="knives/knife">
                        <tr>
                            <td><xsl:value-of select="@knifeId" /></td>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td><xsl:value-of select="visual/blade/bladeLength"/></td>
                            <td><xsl:value-of select="visual/blade/bladeWidth"/></td>
                            <td><xsl:value-of select="visual/blade/bladeMaterial"/></td>
                            <td><xsl:value-of select="visual/hand"/></td>
                            <td><xsl:value-of select="visual/hasRigidityRib"/></td>
                            <td><xsl:value-of select="value"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>