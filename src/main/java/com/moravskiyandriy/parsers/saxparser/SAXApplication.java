package com.moravskiyandriy.parsers.saxparser;

import com.moravskiyandriy.model.ColdWeapon;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Comparator;
import java.util.List;

public class SAXApplication {
    private static final Logger logger = LogManager.getLogger(SAXApplication.class);

    public static void main(String... args) {
        File xmlFile = new File("src\\main\\resources\\xml\\knives.xml");
        File xsdFile = new File("src\\main\\resources\\xml\\knivesXSD.xsd");
        List<ColdWeapon> weaponList = SAXParser.parse(xmlFile, xsdFile);
        weaponList.sort(Comparator.comparing(ColdWeapon::getOrigin).thenComparing(ColdWeapon::getType));
        for (ColdWeapon c : weaponList) {
            logger.info(c);
        }
    }
}
