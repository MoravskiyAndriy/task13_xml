package com.moravskiyandriy.parsers.saxparser;

import com.moravskiyandriy.model.Blade;
import com.moravskiyandriy.model.ColdWeapon;
import com.moravskiyandriy.model.Visual;
import com.moravskiyandriy.xmlvalidator.XMLValidator;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class SAXParser {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

    static List<ColdWeapon> parse(File xml, File xsd) {
        List<ColdWeapon> weapons = new ArrayList<>();
        try {
            saxParserFactory.setSchema(XMLValidator.createSchema(xsd));
            saxParserFactory.setValidating(true);

            javax.xml.parsers.SAXParser saxParser = saxParserFactory.newSAXParser();
            System.out.println(saxParser.isValidating());
            SAXHandler saxHandler = new SAXHandler();
            saxParser.parse(xml, saxHandler);
            weapons = saxHandler.getKnivesList();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
        }
        return weapons;
    }


    private static class SAXHandler extends DefaultHandler {
        static final String KNIVES_TAG = "knives";
        static final String KNIFE_TAG = "knife";
        static final String NAME_TAG = "name";
        static final String ORIGIN_TAG = "origin";
        static final String TYPE_TAG = "type";
        static final String BLADE_TAG = "blade";
        static final String HANDY_TAG = "handy";
        static final String BLADELENGTH_TAG = "bladeLength";
        static final String BLADEWIDTH_TAG = "bladeWidth";
        static final String BLADEMATERIAL_TAG = "bladeMaterial";
        static final String HAND_TAG = "hand";
        static final String HASRIGIDITYRIB_TAG = "hasRigidityRib";
        static final String VISUAL_TAG = "visual";
        static final String VALUE_TAG = "value";
        static final String KNIFE_ID_ATTRIBUTE = "knifeId";

        private List<ColdWeapon> knives;
        private ColdWeapon knife;
        private Visual visual;
        private Blade blade;
        private String currentElement;

        List<ColdWeapon> getKnivesList() {
            return this.knives;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            currentElement = qName;

            switch (currentElement) {
                case KNIVES_TAG: {
                    knives = new ArrayList<>();
                    break;
                }
                case KNIFE_TAG: {
                    String deviceId = attributes.getValue(KNIFE_ID_ATTRIBUTE);
                    knife = new ColdWeapon();
                    knife.setId(Integer.parseInt(deviceId));
                    break;
                }
                case VISUAL_TAG: {
                    visual = new Visual();
                    break;
                }
                case BLADE_TAG: {
                    blade = new Blade();
                    break;
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            switch (qName) {
                case KNIFE_TAG: {
                    knives.add(knife);
                }
                case VISUAL_TAG: {
                    knife.setVisual(visual);
                    break;
                }
                case BLADE_TAG: {
                    visual.setBlade(blade);
                    break;
                }
                case KNIVES_TAG: {
                    break;
                }
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) {
            if (currentElement.equals(NAME_TAG)) {
                knife.setName(new String(ch, start, length));
            }
            if (currentElement.equals(ORIGIN_TAG)) {
                knife.setOrigin(new String(ch, start, length));
            }
            if (currentElement.equals(TYPE_TAG)) {
                knife.setType(new String(ch, start, length));
            }
            if (currentElement.equals(HASRIGIDITYRIB_TAG)) {
                visual.setHasRigidityRib(Boolean.parseBoolean(new String(ch, start, length)));
            }
            if (currentElement.equals(VALUE_TAG)) {
                knife.setValue(Boolean.parseBoolean(new String(ch, start, length)));
            }
            if (currentElement.equals(BLADELENGTH_TAG)) {
                blade.setBladeLength(Double.parseDouble(new String(ch, start, length)));
            }
            if (currentElement.equals(BLADEWIDTH_TAG)) {
                blade.setBladeWidth(Double.parseDouble(new String(ch, start, length)));
            }
            if (currentElement.equals(BLADEMATERIAL_TAG)) {
                blade.setBladeMaterial(new String(ch, start, length));
            }
            if (currentElement.equals(HANDY_TAG)) {
                knife.setHandy(new String(ch, start, length));
            }
            if (currentElement.equals(HAND_TAG)) {
                visual.setHand(new String(ch, start, length));
            }
        }
    }
}
