package com.moravskiyandriy.parsers.domparser;

import com.moravskiyandriy.model.ColdWeapon;
import com.moravskiyandriy.model.Visual;
import com.moravskiyandriy.xmlvalidator.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class DOMApplication {
    private static final Logger logger = LogManager.getLogger(DOMApplication.class);
    private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    public static void main(String... args) {
        File xmlFile = new File("src\\main\\resources\\xml\\knives.xml");
        File xsdFile = new File("src\\main\\resources\\xml\\knivesXSD.xsd");
        Document document = null;
        try {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        if (XMLValidator.validate(document, xsdFile)) {
            List<ColdWeapon> weaponList = DOMParser.parse(document);
            weaponList.sort(Comparator.comparing(ColdWeapon::getVisual, Comparator.comparing(Visual::getHand)));
            for (ColdWeapon c : weaponList) {
                logger.info(c);
            }
        } else {
            logger.info("XML document failed validation.");
        }
    }
}
