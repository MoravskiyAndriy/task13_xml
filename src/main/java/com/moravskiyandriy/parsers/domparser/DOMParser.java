package com.moravskiyandriy.parsers.domparser;

import com.moravskiyandriy.model.Blade;
import com.moravskiyandriy.model.ColdWeapon;
import com.moravskiyandriy.model.Visual;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

class DOMParser {

    static List<ColdWeapon> parse(Document document) {
        List<ColdWeapon> ColdWeapons = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName("knife");
        for (int index = 0; index < nodeList.getLength(); index++) {
            ColdWeapon ColdWeapon = new ColdWeapon();
            Visual visual;
            Node node = nodeList.item(index);

            Element element = (Element) node;
            ColdWeapon.setId(Integer.parseInt(element.getAttribute("knifeId")));
            ColdWeapon.setName(element.getElementsByTagName("name").item(0).getTextContent());
            ColdWeapon.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
            ColdWeapon.setType(element.getElementsByTagName("type").item(0).getTextContent());
            ColdWeapon.setHandy(element.getElementsByTagName("handy").item(0).getTextContent());

            visual = getVisual(element.getElementsByTagName("visual"));
            ColdWeapon.setVisual(visual);
            ColdWeapons.add(ColdWeapon);
        }
        return ColdWeapons;
    }

    private static Visual getVisual(NodeList nodeList) {
        Visual visual = new Visual();
        Blade blade = new Blade();

        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            blade.setBladeLength(Double.parseDouble(element.getElementsByTagName("bladeLength").item(0)
                    .getTextContent()));
            blade.setBladeWidth(Double.parseDouble(element.getElementsByTagName("bladeWidth").item(0)
                    .getTextContent()));
            blade.setBladeMaterial(element.getElementsByTagName("bladeMaterial").item(0).getTextContent());
            visual.setBlade(blade);
            visual.setHand(element.getElementsByTagName("hand").item(0).getTextContent());
            visual.setHasRigidityRib(Boolean.parseBoolean(element.getElementsByTagName("hasRigidityRib").item(0)
                    .getTextContent()));
        }
        return visual;
    }
}
