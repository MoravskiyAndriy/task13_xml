package com.moravskiyandriy.parsers.staxparser;

import com.moravskiyandriy.model.ColdWeapon;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Comparator;
import java.util.List;

public class STAXApplication {
    private static final Logger logger = LogManager.getLogger(STAXApplication.class);

    public static void main(String... args) {
        File xmlFile = new File("src\\main\\resources\\xml\\knives.xml");
        STAXParser staxParser = new STAXParser();
        List<ColdWeapon> weaponList = staxParser.parse(xmlFile);
        weaponList.sort(Comparator.comparing(ColdWeapon::getVisual,
                (k1, k2) -> (int) (k1.getBlade().getBladeLength() * 10 - k2.getBlade().getBladeLength() * 10)));
        for (ColdWeapon c : weaponList) {
            logger.info(c);
        }
    }
}
