package com.moravskiyandriy.parsers.staxparser;

import com.moravskiyandriy.model.Blade;
import com.moravskiyandriy.model.ColdWeapon;
import com.moravskiyandriy.model.Visual;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

class STAXParser {
    private static final String KNIFE_TAG = "knife";
    private static final String NAME_TAG = "name";
    private static final String ORIGIN_TAG = "origin";
    private static final String TYPE_TAG = "type";
    private static final String BLADE_TAG = "blade";
    private static final String HANDY_TAG = "handy";
    private static final String BLADELENGTH_TAG = "bladeLength";
    private static final String BLADEWIDTH_TAG = "bladeWidth";
    private static final String BLADEMATERIAL_TAG = "bladeMaterial";
    private static final String HAND_TAG = "hand";
    private static final String HASRIGIDITYRIB_TAG = "hasRigidityRib";
    private static final String VISUAL_TAG = "visual";
    private static final String VALUE_TAG = "value";
    private static final String KNIFE_ID_ATTRIBUTE = "knifeId";

    private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    private List<ColdWeapon> knives = new ArrayList<>();
    private ColdWeapon knife;
    private Visual visual;
    private Blade blade;

    List<ColdWeapon> parse(File XMLFile) {
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(XMLFile));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    xmlEvent = actionOnElement(xmlEventReader, xmlEvent, startElement, name);

                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    String name = endElement.getName().getLocalPart();
                    actionOnEndElement(name);
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return knives;
    }

    private XMLEvent actionOnElement(XMLEventReader xmlEventReader, XMLEvent xmlEvent, StartElement startElement, String name) throws XMLStreamException {
        switch (name) {
            case KNIFE_TAG: {
                knife = new ColdWeapon();
                Attribute knifeId = startElement.getAttributeByName(new QName(KNIFE_ID_ATTRIBUTE));
                if (knifeId != null) {
                    knife.setId(Integer.parseInt(knifeId.getValue()));
                }
                break;
            }
            case VISUAL_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                visual = new Visual();
                break;
            }
            case BLADE_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                blade = new Blade();
                break;
            }
            case NAME_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                knife.setName(xmlEvent.asCharacters().getData());
                break;
            }
            case ORIGIN_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                knife.setOrigin(xmlEvent.asCharacters().getData());
                break;
            }
            case TYPE_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                knife.setType(xmlEvent.asCharacters().getData());
                break;
            }
            case VALUE_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                knife.setValue(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                break;
            }
            case HASRIGIDITYRIB_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                visual.setHasRigidityRib(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                break;
            }
            case HANDY_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                knife.setHandy(xmlEvent.asCharacters().getData());
                break;
            }
            case BLADELENGTH_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                blade.setBladeLength(Double.parseDouble(xmlEvent.asCharacters().getData()));
                break;
            }
            case BLADEWIDTH_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                blade.setBladeWidth(Double.parseDouble(xmlEvent.asCharacters().getData()));
                break;
            }
            case BLADEMATERIAL_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                blade.setBladeMaterial(xmlEvent.asCharacters().getData());
                break;
            }
            case HAND_TAG: {
                xmlEvent = xmlEventReader.nextEvent();
                visual.setHand(xmlEvent.asCharacters().getData());
                break;
            }
        }
        return xmlEvent;
    }

    private void actionOnEndElement(String name) {
        switch (name) {
            case KNIFE_TAG: {
                knives.add(knife);
                break;
            }
            case VISUAL_TAG: {
                knife.setVisual(visual);
                break;
            }
            case BLADE_TAG: {
                visual.setBlade(blade);
                break;
            }
        }
    }
}
