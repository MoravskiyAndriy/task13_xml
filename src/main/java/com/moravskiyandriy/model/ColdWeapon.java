package com.moravskiyandriy.model;

public class ColdWeapon implements Comparable<ColdWeapon> {
    private int id;
    private String name;
    private String origin;
    private String type;
    private Handy handy;
    private Visual visual;
    private boolean value;

    public ColdWeapon() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Handy getHandy() {
        return handy;
    }

    public void setHandy(String handy) {
        this.handy = Handy.valueOf(handy);
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Visual getVisual() {
        return visual;
    }

    public void setVisual(Visual visual) {
        this.visual = visual;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public int compareTo(ColdWeapon o) {
        return this.id - o.id;
    }

    @Override
    public String toString() {
        return "ColdWeapon{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", type='" + type + '\'' +
                ", handy=" + handy +
                ", visual=" + visual +
                ", value=" + value +
                '}';
    }
}
