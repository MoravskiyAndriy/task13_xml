package com.moravskiyandriy.model;

public enum HandMaterial {
    METAL, PLASTIC, OAK, BIRCH, PINE, MAPLE
}
