package com.moravskiyandriy.model;

public class Blade {
    private double BladeLength;
    private double BladeWidth;
    private String BladeMaterial;

    public Blade() {
    }

    public Blade(double bladeLength, double bladeWidth, String bladeMaterial) {
        BladeLength = bladeLength;
        BladeWidth = bladeWidth;
        BladeMaterial = bladeMaterial;
    }

    public double getBladeLength() {
        return BladeLength;
    }

    public void setBladeLength(double bladeLength) {
        BladeLength = bladeLength;
    }

    public double getBladeWidth() {
        return BladeWidth;
    }

    public void setBladeWidth(double bladeWidth) {
        BladeWidth = bladeWidth;
    }

    public String getBladeMaterial() {
        return BladeMaterial;
    }

    public void setBladeMaterial(String bladeMaterial) {
        BladeMaterial = bladeMaterial;
    }

    @Override
    public String toString() {
        return "Blade{" +
                "BladeLength=" + BladeLength +
                ", BladeWidth=" + BladeWidth +
                ", BladeMaterial='" + BladeMaterial + '\'' +
                '}';
    }
}
