package com.moravskiyandriy.xmltohtml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class Converter {
    private static final Logger logger = LogManager.getLogger(Converter.class);

    public static void main(String args[]) {
        Source xml = new StreamSource(new File("src\\main\\resources\\xml\\knives.xml"));
        Source xslt = new StreamSource("src\\main\\resources\\xml\\knivesXSL.xsl");
        convert(xml, xslt);
    }

    private static void convert(Source xml, Source xslt) {
        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter("src\\main\\resources\\xml\\knives.html");
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(xslt);
            transformer.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            logger.info("knives.html generated successfully.");
        } catch (IOException | TransformerConfigurationException e) {
            logger.info("IOException or TransformerFactoryConfigurationError found:");
            e.printStackTrace();
        } catch (TransformerException e) {
            logger.info("TransformerException found:");
            e.printStackTrace();
        }
    }

}
